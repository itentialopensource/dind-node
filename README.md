# Docker in Docker - Node

[Docker in Docker](https://hub.docker.com/_/docker) + [Nodejs](https://hub.docker.com/_/node) + [Nodejs Build Tools](https://hub.docker.com/_/node).

[This project deploys to docker hub](https://hub.docker.com/r/itentialopensource/dind-node).

## Image Tags

* 8
* 10
* 12
* latest (always the newest version tag of this image)

## Adding Versions

Peruse the [official node builds](https://nodejs.org/dist/). Add the desired version to the `node_versions.csv` file.

  1. The first entry should be the full version of nodejs to include.
  1. The second entry is the version of npm to include.
  1. The third entry is the version of yarn to include.

Run the image updater script to build and test the new image.

```bash
bash build.sh -b
```

Commit your changes, update the `.gitlab-ci.yml` jobs, and make a pull request.
